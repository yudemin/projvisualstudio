﻿namespace exam
{
    partial class menu
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.m1start = new System.Windows.Forms.Button();
            this.m2start = new System.Windows.Forms.Button();
            this.m3start = new System.Windows.Forms.Button();
            this.bset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m1start
            // 
            this.m1start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m1start.Location = new System.Drawing.Point(198, 55);
            this.m1start.Name = "m1start";
            this.m1start.Size = new System.Drawing.Size(290, 81);
            this.m1start.TabIndex = 0;
            this.m1start.Text = "mode 1";
            this.m1start.UseVisualStyleBackColor = true;
            this.m1start.Click += new System.EventHandler(this.m1start_Click);
            // 
            // m2start
            // 
            this.m2start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m2start.Location = new System.Drawing.Point(198, 142);
            this.m2start.Name = "m2start";
            this.m2start.Size = new System.Drawing.Size(290, 81);
            this.m2start.TabIndex = 1;
            this.m2start.Text = "mode 2";
            this.m2start.UseVisualStyleBackColor = true;
            this.m2start.Click += new System.EventHandler(this.m2start_Click);
            // 
            // m3start
            // 
            this.m3start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m3start.Location = new System.Drawing.Point(198, 229);
            this.m3start.Name = "m3start";
            this.m3start.Size = new System.Drawing.Size(290, 81);
            this.m3start.TabIndex = 2;
            this.m3start.Text = "mode 3";
            this.m3start.UseVisualStyleBackColor = true;
            this.m3start.Click += new System.EventHandler(this.m3start_Click);
            // 
            // bset
            // 
            this.bset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bset.Location = new System.Drawing.Point(218, 323);
            this.bset.Name = "bset";
            this.bset.Size = new System.Drawing.Size(253, 56);
            this.bset.TabIndex = 3;
            this.bset.Text = "settings";
            this.bset.UseVisualStyleBackColor = true;
            this.bset.Click += new System.EventHandler(this.bset_Click);
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 391);
            this.Controls.Add(this.bset);
            this.Controls.Add(this.m3start);
            this.Controls.Add(this.m2start);
            this.Controls.Add(this.m1start);
            this.MaximumSize = new System.Drawing.Size(700, 430);
            this.MinimumSize = new System.Drawing.Size(700, 430);
            this.Name = "menu";
            this.Text = "menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.app_exit);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m1start;
        private System.Windows.Forms.Button m2start;
        private System.Windows.Forms.Button m3start;
        private System.Windows.Forms.Button bset;
    }
}


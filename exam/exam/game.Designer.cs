﻿namespace exam
{
    partial class game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lablevel = new System.Windows.Forms.Label();
            this.labtime = new System.Windows.Forms.Label();
            this.labscore = new System.Windows.Forms.Label();
            this.pb12 = new System.Windows.Forms.PictureBox();
            this.pb11 = new System.Windows.Forms.PictureBox();
            this.pb10 = new System.Windows.Forms.PictureBox();
            this.pb9 = new System.Windows.Forms.PictureBox();
            this.pb8 = new System.Windows.Forms.PictureBox();
            this.pb7 = new System.Windows.Forms.PictureBox();
            this.pb6 = new System.Windows.Forms.PictureBox();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.b11 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b13 = new System.Windows.Forms.Button();
            this.b14 = new System.Windows.Forms.Button();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.b31 = new System.Windows.Forms.Button();
            this.b32 = new System.Windows.Forms.Button();
            this.b33 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.labcolor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // lablevel
            // 
            this.lablevel.AutoSize = true;
            this.lablevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lablevel.Location = new System.Drawing.Point(73, 9);
            this.lablevel.Name = "lablevel";
            this.lablevel.Size = new System.Drawing.Size(152, 26);
            this.lablevel.TabIndex = 0;
            this.lablevel.Text = "Level <> of <>";
            // 
            // labtime
            // 
            this.labtime.AutoSize = true;
            this.labtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labtime.Location = new System.Drawing.Point(322, 9);
            this.labtime.Name = "labtime";
            this.labtime.Size = new System.Drawing.Size(66, 26);
            this.labtime.TabIndex = 1;
            this.labtime.Text = "00:00";
            // 
            // labscore
            // 
            this.labscore.AutoSize = true;
            this.labscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labscore.Location = new System.Drawing.Point(533, 9);
            this.labscore.Name = "labscore";
            this.labscore.Size = new System.Drawing.Size(69, 26);
            this.labscore.TabIndex = 2;
            this.labscore.Text = "Score";
            // 
            // pb12
            // 
            this.pb12.Location = new System.Drawing.Point(444, 213);
            this.pb12.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb12.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb12.Name = "pb12";
            this.pb12.Size = new System.Drawing.Size(90, 60);
            this.pb12.TabIndex = 22;
            this.pb12.TabStop = false;
            // 
            // pb11
            // 
            this.pb11.Location = new System.Drawing.Point(348, 213);
            this.pb11.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb11.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb11.Name = "pb11";
            this.pb11.Size = new System.Drawing.Size(90, 60);
            this.pb11.TabIndex = 21;
            this.pb11.TabStop = false;
            // 
            // pb10
            // 
            this.pb10.Location = new System.Drawing.Point(252, 213);
            this.pb10.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb10.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb10.Name = "pb10";
            this.pb10.Size = new System.Drawing.Size(90, 60);
            this.pb10.TabIndex = 20;
            this.pb10.TabStop = false;
            // 
            // pb9
            // 
            this.pb9.Location = new System.Drawing.Point(156, 213);
            this.pb9.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb9.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb9.Name = "pb9";
            this.pb9.Size = new System.Drawing.Size(90, 60);
            this.pb9.TabIndex = 19;
            this.pb9.TabStop = false;
            // 
            // pb8
            // 
            this.pb8.Location = new System.Drawing.Point(444, 147);
            this.pb8.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb8.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb8.Name = "pb8";
            this.pb8.Size = new System.Drawing.Size(90, 60);
            this.pb8.TabIndex = 18;
            this.pb8.TabStop = false;
            // 
            // pb7
            // 
            this.pb7.Location = new System.Drawing.Point(348, 147);
            this.pb7.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb7.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb7.Name = "pb7";
            this.pb7.Size = new System.Drawing.Size(90, 60);
            this.pb7.TabIndex = 17;
            this.pb7.TabStop = false;
            // 
            // pb6
            // 
            this.pb6.Location = new System.Drawing.Point(252, 147);
            this.pb6.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb6.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(90, 60);
            this.pb6.TabIndex = 16;
            this.pb6.TabStop = false;
            // 
            // pb5
            // 
            this.pb5.Location = new System.Drawing.Point(156, 147);
            this.pb5.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb5.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(90, 60);
            this.pb5.TabIndex = 13;
            this.pb5.TabStop = false;
            // 
            // pb3
            // 
            this.pb3.Location = new System.Drawing.Point(348, 81);
            this.pb3.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb3.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(90, 60);
            this.pb3.TabIndex = 15;
            this.pb3.TabStop = false;
            // 
            // pb4
            // 
            this.pb4.Location = new System.Drawing.Point(444, 81);
            this.pb4.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb4.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(90, 60);
            this.pb4.TabIndex = 14;
            this.pb4.TabStop = false;
            // 
            // pb2
            // 
            this.pb2.Location = new System.Drawing.Point(252, 81);
            this.pb2.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb2.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(90, 60);
            this.pb2.TabIndex = 13;
            this.pb2.TabStop = false;
            // 
            // pb1
            // 
            this.pb1.Location = new System.Drawing.Point(156, 81);
            this.pb1.MaximumSize = new System.Drawing.Size(90, 60);
            this.pb1.MinimumSize = new System.Drawing.Size(90, 60);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(90, 60);
            this.pb1.TabIndex = 12;
            this.pb1.TabStop = false;
            this.pb1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // b11
            // 
            this.b11.Location = new System.Drawing.Point(231, 281);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(48, 45);
            this.b11.TabIndex = 4;
            this.b11.Text = "<1>";
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Visible = false;
            this.b11.Click += new System.EventHandler(this.ans_check_1);
            // 
            // b12
            // 
            this.b12.Location = new System.Drawing.Point(285, 281);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(49, 45);
            this.b12.TabIndex = 5;
            this.b12.Text = "<2>";
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Visible = false;
            this.b12.Click += new System.EventHandler(this.ans_check_1);
            // 
            // b13
            // 
            this.b13.Location = new System.Drawing.Point(340, 281);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(48, 45);
            this.b13.TabIndex = 6;
            this.b13.Text = "<3>";
            this.b13.UseVisualStyleBackColor = true;
            this.b13.Visible = false;
            this.b13.Click += new System.EventHandler(this.ans_check_1);
            // 
            // b14
            // 
            this.b14.Location = new System.Drawing.Point(394, 281);
            this.b14.Name = "b14";
            this.b14.Size = new System.Drawing.Size(46, 45);
            this.b14.TabIndex = 7;
            this.b14.Text = "<4>";
            this.b14.UseVisualStyleBackColor = true;
            this.b14.Visible = false;
            this.b14.Click += new System.EventHandler(this.ans_check_1);
            // 
            // tb21
            // 
            this.tb21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb21.Location = new System.Drawing.Point(231, 332);
            this.tb21.MaximumSize = new System.Drawing.Size(209, 26);
            this.tb21.MinimumSize = new System.Drawing.Size(209, 26);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(209, 26);
            this.tb21.TabIndex = 8;
            this.tb21.Text = "<0>";
            this.tb21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb21.Visible = false;
            this.tb21.TextChanged += new System.EventHandler(this.tb21_TextChanged);
            // 
            // b31
            // 
            this.b31.Location = new System.Drawing.Point(164, 364);
            this.b31.Name = "b31";
            this.b31.Size = new System.Drawing.Size(115, 27);
            this.b31.TabIndex = 9;
            this.b31.Text = "Green";
            this.b31.UseVisualStyleBackColor = true;
            this.b31.Visible = false;
            this.b31.Click += new System.EventHandler(this.ans_check_3);
            // 
            // b32
            // 
            this.b32.Location = new System.Drawing.Point(285, 364);
            this.b32.Name = "b32";
            this.b32.Size = new System.Drawing.Size(112, 27);
            this.b32.TabIndex = 10;
            this.b32.Text = "Equally";
            this.b32.UseVisualStyleBackColor = true;
            this.b32.Visible = false;
            this.b32.Click += new System.EventHandler(this.ans_check_3);
            // 
            // b33
            // 
            this.b33.Location = new System.Drawing.Point(403, 364);
            this.b33.Name = "b33";
            this.b33.Size = new System.Drawing.Size(121, 27);
            this.b33.TabIndex = 11;
            this.b33.Text = "Red";
            this.b33.UseVisualStyleBackColor = true;
            this.b33.Visible = false;
            this.b33.Click += new System.EventHandler(this.ans_check_3);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // labcolor
            // 
            this.labcolor.AutoSize = true;
            this.labcolor.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labcolor.Location = new System.Drawing.Point(226, 52);
            this.labcolor.Name = "labcolor";
            this.labcolor.Size = new System.Drawing.Size(248, 26);
            this.labcolor.TabIndex = 23;
            this.labcolor.Text = "How many red squares?";
            // 
            // game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 391);
            this.Controls.Add(this.labcolor);
            this.Controls.Add(this.pb12);
            this.Controls.Add(this.b33);
            this.Controls.Add(this.pb11);
            this.Controls.Add(this.b32);
            this.Controls.Add(this.pb10);
            this.Controls.Add(this.b31);
            this.Controls.Add(this.pb9);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.pb8);
            this.Controls.Add(this.b14);
            this.Controls.Add(this.pb7);
            this.Controls.Add(this.b13);
            this.Controls.Add(this.pb6);
            this.Controls.Add(this.b12);
            this.Controls.Add(this.pb5);
            this.Controls.Add(this.b11);
            this.Controls.Add(this.pb3);
            this.Controls.Add(this.pb4);
            this.Controls.Add(this.pb2);
            this.Controls.Add(this.labscore);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.labtime);
            this.Controls.Add(this.lablevel);
            this.MaximumSize = new System.Drawing.Size(700, 430);
            this.MinimumSize = new System.Drawing.Size(700, 430);
            this.Name = "game";
            this.Text = "game";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formswith);
            this.Load += new System.EventHandler(this.game_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lablevel;
        private System.Windows.Forms.Label labtime;
        private System.Windows.Forms.Label labscore;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b13;
        private System.Windows.Forms.Button b14;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.Button b31;
        private System.Windows.Forms.Button b32;
        private System.Windows.Forms.Button b33;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb12;
        private System.Windows.Forms.PictureBox pb11;
        private System.Windows.Forms.PictureBox pb10;
        private System.Windows.Forms.PictureBox pb9;
        private System.Windows.Forms.PictureBox pb8;
        private System.Windows.Forms.PictureBox pb7;
        private System.Windows.Forms.PictureBox pb6;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label labcolor;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using examlib;
//using XColor = Microsoft.Xna.Framework.Graphics.Color;


namespace exam
{
    public partial class game : Form
    {
        public game()
        {
            InitializeComponent();
            gamestart(objprop.mode);
            coloring();
            ans_set();
            lablevel.Text = String.Format("Level {0}", objprop.level.ToString());
            labtime.Text = String.Format("Time 0:{0}", objprop.time.ToString());
            labscore.Text = String.Format("Score {0}", objprop.score.ToString());
            tb21.KeyDown += new KeyEventHandler(tb_KeyDown);
        }
       public void refresh()
        {
            objprop.level++;
            objprop.score += objprop.level * 2;
            labscore.Text = String.Format("Score {0}", objprop.score.ToString());
            lablevel.Text = String.Format("Level {0}", objprop.level.ToString());
            MessageBox.Show("Yes");
            coloring();
        }

        public void gamegebin(int mode)
        {
            //b11.Visible = true;
        }
        public void gamestart(int mode)
        {
            if (mode == 1)
            {
                b11.Visible = b12.Visible = b13.Visible = b14.Visible = true;                
            }
            if (mode == 2)
            {
                tb21.Visible = true;
            }
            if (mode == 3)
            {
                b31.Visible = b32.Visible = b33.Visible = true;
            }
        }

        public void coloring()
        {
            PictureBox[] boxes = { pb1, pb2, pb3, pb4, pb5, pb6, pb7, pb8, pb9, pb10, pb11, pb12 };
            core.col_gen();
            for (int i = 0; i < 12; i++)
            {
                Color col = Color.FromName(objprop.colors[i]);
                boxes[i].BackColor = col;
            }
            labcolor.Text = String.Format("How many {0} squares ?", objprop.square_color_3);
        }
        public void ans_set()
        {
            timer.Enabled = true;            
            objprop.timer_en = true;
            core.answ_gen();
            core.color_compare();
            Button[] but = { b11, b12, b13, b14 };
            for (int i = 0; i< objprop.answ_array.Length; i++)
            {
                //MessageBox.Show(objprop.answ_array[i].ToString());
                but[i].Text = objprop.answ_array[i].ToString();
            }
        }

        public void ans_check_1(object sender, EventArgs e)
        {
            Button b = (sender as Button);            
            //MessageBox.Show(b.Text.ToString()+ "--"+ objprop.square_color_3.ToString());
            if (b.Text.ToString() == objprop.color_3_count.ToString())
            {
                refresh();
                ans_set();            
            } else
            {
                MessageBox.Show("No");
            }
        }
        
        public void ans_check_2()
        {    
            //MessageBox.Show(b.Text.ToString()+ "--"+ objprop.square_color_3.ToString());
            if (tb21.Text.ToString() == objprop.color_3_count.ToString())
            {
                tb21.Text = "";
                refresh();
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void game_Load(object sender, EventArgs e)
        {

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (objprop.timer_en == true && objprop.time > 0)
            {
                core.timer_tick();
            } else
            {
                timer.Enabled = false;
                MessageBox.Show("YOU LOSE");
                this.Close();
            }            
            labtime.Text = String.Format("Time 0:{0}", objprop.time.ToString());
        }

        void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (tb21.Text == "") { } else { ans_check_2(); }
            }
        }
        private void tb21_TextChanged(object sender, EventArgs e){}

        public void ans_check_3(object sender, EventArgs e)
        {
            Button b = (sender as Button);
            string answ = "";
            int answ_comp = core.color_compare();
            
            if (answ_comp == 1)
            {
                answ = "Equally";
            } else if (answ_comp == 2)
            {
                answ = objprop.square_color_2.ToString();
            } else if (answ_comp == 3)
            {
                answ = objprop.square_color_3.ToString();
            }
           // MessageBox.Show(core.color_compare().ToString()+answ);
            //MessageBox.Show(b.Text.ToString()+ "--"+ objprop.square_color_3.ToString());
            if (b.Text.ToString() == answ)
            {
                refresh();
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        
        private void formswith(object sender, FormClosingEventArgs e)
        {
            core.refresh();
            menu menu = new menu();
            menu.Show();
        }
    }
}

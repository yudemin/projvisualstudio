﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using examlib;

namespace exam
{
    public partial class settigs : Form
    {
        public settigs()
        {
            InitializeComponent();
            cb1.Items.Add("Red"); cb1.Items.Add("Blue"); cb1.Items.Add("Green"); cb1.Items.Add("Yellow");
            cb2.Items.Add("Red"); cb2.Items.Add("Blue"); cb2.Items.Add("Green"); cb2.Items.Add("Yellow");
        }

        public void warn_check()
        {
            if (cb1.SelectedItem == cb2.SelectedItem)
            {
                warning1.Visible = true;
            }
            else
            {
                warning1.Visible = false;
            }
        }

        private void formswitch(object sender, FormClosingEventArgs e)
        {
            objprop.square_color_2 = cb1.SelectedItem.ToString();
            objprop.square_color_3 = cb2.SelectedItem.ToString();
            menu menu = new menu();
            menu.Show();
        }

        private void settigs_Load(object sender, EventArgs e)
        {
            cb1.SelectedIndex = 0;
            cb2.SelectedIndex = 2;
        }



        private void cb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            warn_check();
        }

        private void cb2_SelectedIndexChanged(object sender, EventArgs e)
        {
            warn_check();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using examlib;

namespace exam
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }
        
    private void swithform()
        {            
            game game = new game();
            game.Show();
            this.Hide();
        }

        private void m1start_Click(object sender, EventArgs e)
        {
            objprop.mode = 1;
            swithform();            
        }

        private void m2start_Click(object sender, EventArgs e)
        {
            objprop.mode = 2;
            swithform();            
        }

        private void m3start_Click(object sender, EventArgs e)
        {
            objprop.mode = 3;
            swithform();            
        }

        private void app_exit(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void bset_Click(object sender, EventArgs e)
        {
            settigs settigs = new settigs();
            settigs.Show();
            this.Hide();
        }
    }
}

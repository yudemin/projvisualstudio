﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.ComponentModel;
using System.Data;
using System.Drawing;

using examlib;

namespace examlib
{
    public class core
    {
        public static void refresh()
        {
            objprop.level = 1;
            objprop.score = 0;
            objprop.time = 60;
        }
        public static void col_gen()
        {
            int col_id;
            Random random = new Random();

            //Color c1 = Color.FromName("Red");
            objprop.color_1_count = objprop.color_2_count = objprop.color_3_count = 0;

            for (int i = 0; i < 12; i++)
            {
                col_id = random.Next(3);
                if (col_id == 0) {
                    objprop.colors[i] = objprop.square_color_1;
                    objprop.color_1_count++;
                }
                if (col_id == 1) {
                    objprop.colors[i] = objprop.square_color_2;
                    objprop.color_2_count++;
                }
                if (col_id == 2) {
                    objprop.colors[i] = objprop.square_color_3;
                    objprop.color_3_count++;
                }
            }
            //and set timer value
            if ((60 - objprop.level * 5) > 5)
            {
                objprop.time = 60 - objprop.level * 5;
            }
            else
            {
                objprop.time = 5;
            }
        }
        public static void answ_gen()
        {
            int answ = objprop.color_3_count;
            objprop.answ_array[0] = answ;
            Random random = new Random();
            int diff;
            for (int i = 1; i< objprop.answ_array.Length; i++)
            {
                do
                {
                    diff = random.Next(-2, 4);
                    objprop.answ_array[i] = answ + diff;
                }
                while (objprop.answ_array[i] == answ);
            }
            shuffle(objprop.answ_array);
        }
        public static void shuffle(int[] answ_array)
        {
            Random random = new Random();
            for (int t = 0; t < answ_array.Length; t++)
            {
                int tmp = answ_array[t];
                int r = random.Next(t, answ_array.Length);
                answ_array[t] = answ_array[r];
                answ_array[r] = tmp;
            }
        }
        public static void timer_tick()
        {
            if (objprop.time == 0)
            {
                objprop.timer_en = false;
            }            
            objprop.time--;
        }
        public static int color_compare()
        {
            int green = objprop.color_2_count;
            int red = objprop.color_3_count;
            if (green == red)
            {
                return 1;
            } else if (green > red)
            {
                return 2;
            } else if (green < red)
            {
                return 3;
            }
            return 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using examlib;


namespace examlib
{
    public class objprop
    {
        public static int mode = 0;
        public static int level = 1;
        public static int score = 0;

        public static string square_color_1 = "White";
        public static string square_color_2 = "Green";
        public static string square_color_3 = "Red";
        public static int color_1_count = 0;
        public static int color_2_count = 0;
        public static int color_3_count = 0;
        public static int time = 60;
        public static bool timer_en = false;
        public static int[] answ_array = new int[4];
        public static string[] colors = new string[12];
    }
}

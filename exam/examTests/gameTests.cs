﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using exam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using examlib;

namespace exam.Tests
{
    [TestClass()]
    public class gameTests
    {
        [TestMethod()]
        public void compare_check_1()
        {
            objprop.color_2_count = 15;
            objprop.color_3_count = 15;
            int chek_1 = core.color_compare();
            Assert.AreEqual(1, chek_1);           
        }
        [TestMethod()]
        public void compare_check_2()
        {
            objprop.color_2_count = 20;
            objprop.color_3_count = 10;
            int chek_2 = core.color_compare();
            Assert.AreEqual(2, chek_2);           
        }
        [TestMethod()]
        public void compare_check_3()
        {
            objprop.color_2_count = 10;
            objprop.color_3_count = 20;
            int chek_3 = core.color_compare();
            Assert.AreEqual(3, chek_3);           
        }
        [TestMethod()]
        public void refresh_check()
        {
            objprop.level = 25;
            objprop.score = 9999;
            objprop.time = 6;
            core.refresh();
            Assert.AreEqual(1, objprop.level);
            Assert.AreEqual(0, objprop.score);
            Assert.AreEqual(60, objprop.time);
        }
        [TestMethod()]
        public void timer_tick_check()
        {
            objprop.timer_en = true;
            objprop.time = 0;
            core.timer_tick();
            Assert.AreEqual(false, objprop.timer_en);    
        }
        [TestMethod()]
        public void col_gen_check()
        {
            objprop.colors = new string[12];            
            core.col_gen();
            Assert.AreNotEqual(null, objprop.colors[10]);
        }
    }

    
}
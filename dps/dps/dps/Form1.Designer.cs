﻿namespace dps
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.hpbar = new System.Windows.Forms.ProgressBar();
            this.manabar = new System.Windows.Forms.ProgressBar();
            this.hpbar_boss = new System.Windows.Forms.ProgressBar();
            this.but4 = new System.Windows.Forms.Button();
            this.end_turn = new System.Windows.Forms.Button();
            this.hp_player_value = new System.Windows.Forms.Label();
            this.mana_player_value = new System.Windows.Forms.Label();
            this.hp_boss_value = new System.Windows.Forms.Label();
            this.timer_msg = new System.Windows.Forms.Timer(this.components);
            this.pic_vic = new System.Windows.Forms.Label();
            this.pic_def = new System.Windows.Forms.Label();
            this.pic_boss = new System.Windows.Forms.PictureBox();
            this.but5 = new System.Windows.Forms.Button();
            this.but3 = new System.Windows.Forms.Button();
            this.but2 = new System.Windows.Forms.Button();
            this.but1 = new System.Windows.Forms.Button();
            this.timer_num = new System.Windows.Forms.Timer(this.components);
            this.num = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_boss)).BeginInit();
            this.SuspendLayout();
            // 
            // hpbar
            // 
            this.hpbar.BackColor = System.Drawing.SystemColors.Control;
            this.hpbar.ForeColor = System.Drawing.Color.Red;
            this.hpbar.Location = new System.Drawing.Point(12, 12);
            this.hpbar.Name = "hpbar";
            this.hpbar.Size = new System.Drawing.Size(168, 15);
            this.hpbar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.hpbar.TabIndex = 0;
            // 
            // manabar
            // 
            this.manabar.BackColor = System.Drawing.Color.Cornsilk;
            this.manabar.ForeColor = System.Drawing.Color.MediumBlue;
            this.manabar.Location = new System.Drawing.Point(12, 33);
            this.manabar.Name = "manabar";
            this.manabar.Size = new System.Drawing.Size(168, 15);
            this.manabar.TabIndex = 1;
            // 
            // hpbar_boss
            // 
            this.hpbar_boss.BackColor = System.Drawing.SystemColors.Control;
            this.hpbar_boss.ForeColor = System.Drawing.Color.MediumBlue;
            this.hpbar_boss.Location = new System.Drawing.Point(12, 260);
            this.hpbar_boss.Name = "hpbar_boss";
            this.hpbar_boss.Size = new System.Drawing.Size(327, 21);
            this.hpbar_boss.TabIndex = 4;
            // 
            // but4
            // 
            this.but4.BackgroundImage = global::dps.Properties.Resources.heal;
            this.but4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.but4.Location = new System.Drawing.Point(71, 287);
            this.but4.Name = "but4";
            this.but4.Size = new System.Drawing.Size(53, 52);
            this.but4.TabIndex = 8;
            this.but4.UseVisualStyleBackColor = true;
            this.but4.Click += new System.EventHandler(this.but4_Click);
            // 
            // end_turn
            // 
            this.end_turn.Location = new System.Drawing.Point(247, 319);
            this.end_turn.Name = "end_turn";
            this.end_turn.Size = new System.Drawing.Size(92, 78);
            this.end_turn.TabIndex = 9;
            this.end_turn.Text = "end_turn";
            this.end_turn.UseVisualStyleBackColor = true;
            this.end_turn.Click += new System.EventHandler(this.end_turn_Click);
            // 
            // hp_player_value
            // 
            this.hp_player_value.AutoSize = true;
            this.hp_player_value.Location = new System.Drawing.Point(186, 12);
            this.hp_player_value.Name = "hp_player_value";
            this.hp_player_value.Size = new System.Drawing.Size(36, 13);
            this.hp_player_value.TabIndex = 10;
            this.hp_player_value.Text = "0/100";
            // 
            // mana_player_value
            // 
            this.mana_player_value.AutoSize = true;
            this.mana_player_value.Location = new System.Drawing.Point(186, 35);
            this.mana_player_value.Name = "mana_player_value";
            this.mana_player_value.Size = new System.Drawing.Size(36, 13);
            this.mana_player_value.TabIndex = 11;
            this.mana_player_value.Text = "0/100";
            // 
            // hp_boss_value
            // 
            this.hp_boss_value.AutoSize = true;
            this.hp_boss_value.Location = new System.Drawing.Point(156, 260);
            this.hp_boss_value.Name = "hp_boss_value";
            this.hp_boss_value.Size = new System.Drawing.Size(36, 13);
            this.hp_boss_value.TabIndex = 12;
            this.hp_boss_value.Text = "0/100";
            // 
            // timer_msg
            // 
            this.timer_msg.Interval = 3000;
            this.timer_msg.Tick += new System.EventHandler(this.timer_msg_Tick);
            // 
            // pic_vic
            // 
            this.pic_vic.AutoSize = true;
            this.pic_vic.BackColor = System.Drawing.Color.Lime;
            this.pic_vic.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pic_vic.ForeColor = System.Drawing.Color.Black;
            this.pic_vic.Location = new System.Drawing.Point(74, 127);
            this.pic_vic.Name = "pic_vic";
            this.pic_vic.Size = new System.Drawing.Size(218, 46);
            this.pic_vic.TabIndex = 13;
            this.pic_vic.Text = "VICTORY !";
            this.pic_vic.Visible = false;
            // 
            // pic_def
            // 
            this.pic_def.AutoSize = true;
            this.pic_def.BackColor = System.Drawing.Color.Red;
            this.pic_def.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pic_def.ForeColor = System.Drawing.Color.Black;
            this.pic_def.Location = new System.Drawing.Point(99, 127);
            this.pic_def.Name = "pic_def";
            this.pic_def.Size = new System.Drawing.Size(178, 46);
            this.pic_def.TabIndex = 14;
            this.pic_def.Text = "DEFEAT";
            this.pic_def.Visible = false;
            // 
            // pic_boss
            // 
            this.pic_boss.Image = global::dps.Properties.Resources._5;
            this.pic_boss.Location = new System.Drawing.Point(12, 54);
            this.pic_boss.Name = "pic_boss";
            this.pic_boss.Size = new System.Drawing.Size(327, 200);
            this.pic_boss.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_boss.TabIndex = 15;
            this.pic_boss.TabStop = false;
            // 
            // but5
            // 
            this.but5.BackgroundImage = global::dps.Properties.Resources.light;
            this.but5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.but5.Location = new System.Drawing.Point(130, 345);
            this.but5.Name = "but5";
            this.but5.Size = new System.Drawing.Size(53, 52);
            this.but5.TabIndex = 7;
            this.but5.UseVisualStyleBackColor = true;
            this.but5.Click += new System.EventHandler(this.but5_Click);
            // 
            // but3
            // 
            this.but3.BackgroundImage = global::dps.Properties.Resources.unleashweapon;
            this.but3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.but3.Location = new System.Drawing.Point(12, 287);
            this.but3.Name = "but3";
            this.but3.Size = new System.Drawing.Size(53, 52);
            this.but3.TabIndex = 6;
            this.but3.UseVisualStyleBackColor = true;
            this.but3.Click += new System.EventHandler(this.but3_Click);
            // 
            // but2
            // 
            this.but2.BackgroundImage = global::dps.Properties.Resources.Lava;
            this.but2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.but2.Location = new System.Drawing.Point(71, 345);
            this.but2.Name = "but2";
            this.but2.Size = new System.Drawing.Size(53, 52);
            this.but2.TabIndex = 5;
            this.but2.UseVisualStyleBackColor = true;
            this.but2.Click += new System.EventHandler(this.but2_Click);
            // 
            // but1
            // 
            this.but1.BackgroundImage = global::dps.Properties.Resources.SS;
            this.but1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.but1.Location = new System.Drawing.Point(12, 345);
            this.but1.Name = "but1";
            this.but1.Size = new System.Drawing.Size(53, 52);
            this.but1.TabIndex = 3;
            this.but1.UseVisualStyleBackColor = true;
            this.but1.Click += new System.EventHandler(this.but1_Click_1);
            // 
            // timer_num
            // 
            this.timer_num.Interval = 1000;
            this.timer_num.Tick += new System.EventHandler(this.timer_num_Tick);
            // 
            // num
            // 
            this.num.AutoSize = true;
            this.num.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.num.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.num.Location = new System.Drawing.Point(250, 200);
            this.num.Name = "num";
            this.num.Size = new System.Drawing.Size(66, 31);
            this.num.TabIndex = 16;
            this.num.Text = "num";
            this.num.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 415);
            this.Controls.Add(this.num);
            this.Controls.Add(this.pic_boss);
            this.Controls.Add(this.pic_def);
            this.Controls.Add(this.pic_vic);
            this.Controls.Add(this.hp_boss_value);
            this.Controls.Add(this.mana_player_value);
            this.Controls.Add(this.hp_player_value);
            this.Controls.Add(this.end_turn);
            this.Controls.Add(this.but4);
            this.Controls.Add(this.but5);
            this.Controls.Add(this.but3);
            this.Controls.Add(this.but2);
            this.Controls.Add(this.hpbar_boss);
            this.Controls.Add(this.but1);
            this.Controls.Add(this.manabar);
            this.Controls.Add(this.hpbar);
            this.Name = "Form1";
            this.Text = "WoW";
            ((System.ComponentModel.ISupportInitialize)(this.pic_boss)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar hpbar;
        private System.Windows.Forms.ProgressBar manabar;
        private System.Windows.Forms.Button but1;
        private System.Windows.Forms.ProgressBar hpbar_boss;
        private System.Windows.Forms.Button but2;
        private System.Windows.Forms.Button but3;
        private System.Windows.Forms.Button but5;
        private System.Windows.Forms.Button but4;
        private System.Windows.Forms.Button end_turn;
        private System.Windows.Forms.Label hp_player_value;
        private System.Windows.Forms.Label mana_player_value;
        private System.Windows.Forms.Label hp_boss_value;
        private System.Windows.Forms.Timer timer_msg;
        private System.Windows.Forms.Label pic_vic;
        private System.Windows.Forms.Label pic_def;
        private System.Windows.Forms.PictureBox pic_boss;
        private System.Windows.Forms.Timer timer_num;
        private System.Windows.Forms.Label num;
    }
}


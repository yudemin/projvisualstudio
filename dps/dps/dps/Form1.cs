﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Resources;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using scada;

namespace dps
{
    public  partial class Form1 : Form
    {
        private window w;
        public Form1()
        {
            InitializeComponent();
            w = new window();
            hpbar.Value = w.hp_player;
            manabar.Value = w.mana_player;
            hpbar_boss.Value = w.hp_boss;
            start();

        }

        int cd_1;
        int cd_2;
        int cd_3;
        int cd_4;
        int cd_5;
        // int boss_counter = 0;
        bool burst = false;
   
        public void start()
        {
            w.hp_boss = 100;
            w.hp_player = 100;
            w.mana_player = 100;
            Random picrnd = new Random();
            int number = picrnd.Next(1, 5); // max = quantity of bosses
            string file = number.ToString();
            //string file = String.Format("{0}.jpg", number);
            // pic_boss.Image = System.Drawing.Image.FromFile(file);
            pic_boss.Image = (Image)Properties.Resources.ResourceManager.GetObject(file);
            cd_1 = 0; cd_2 = 0; cd_3 = 0; cd_4 = 0; cd_5 = 0;
            but1.Visible = true;but2.Visible = true;but3.Visible = true;but4.Visible = true;but5.Visible = true;
            num.Visible = false;
            pic_boss.Visible = true;
            refresh();
        }


       public void boss_take_dmg(int damage)
        {
            if (w.hp_boss - damage <= 0)
            {
                w.hp_boss = 0;
                pic_boss.Visible = false;
                pic_vic.Visible = true;
                timer_msg.Enabled = true;
                this.Enabled = false;

            } else
            {
                w.hp_boss -= damage;
                
            }
            num_show(damage, 1);
        }

        public void num_show(int value, int color)
        {
            Random numpos = new Random();
            int numx = numpos.Next(35, 250);
            int numy = numpos.Next(80, 200);
            num.Location = new Point(numx, numy);
            if (color == 1)
            {
                num.ForeColor = System.Drawing.Color.Red;
                num.Text = String.Format("-{0}", value);
                num.Visible = true;
                timer_num.Enabled = true;
                //num.Location.X = new 
            } else if (color == 2)
            {
                num.ForeColor = System.Drawing.Color.Green;
                num.Text = String.Format("+{0}", value);
                num.Visible = true;
                timer_num.Enabled = true;
            }
        }

        public void mana_spend(int cost)
        {
            if (w.mana_player - cost <= 0)
            {
                w.hp_boss = 0;
            }
            else
            {
                w.mana_player -= cost;
            }
        }
        public void refresh()
        {
            hpbar.Value = w.hp_player;
            manabar.Value = w.mana_player;
            hpbar_boss.Value = w.hp_boss;
            hp_player_value.Text = String.Format("hp: {0}/100", w.hp_player);
            mana_player_value.Text = String.Format("mana: {0}/100", w.mana_player);
            hp_boss_value.Text = String.Format("hp: {0}/100", w.hp_boss);
        }

        private void but1_Click(object sender, EventArgs e)
        {
            // error!! dont look, baka
        }

        private void but1_Click_1(object sender, EventArgs e)
        {
            // shtorm strike
            Random ssrnd = new Random();
            int ssdmg = ssrnd.Next(5, 40);
            mana_spend(20);
            boss_take_dmg(ssdmg);
            cd_1 = 3;
            but1.Visible = false;
            
            refresh();
        }

        private void but2_Click(object sender, EventArgs e)
        {
            // boiling up of lava
            mana_spend(5);
            if (burst == true)
            {
                boss_take_dmg(24);
            } else
            {
                boss_take_dmg(8);
            }           
            cd_2 = 3;
            but2.Visible = false;
            refresh();
        }

        private void but3_Click(object sender, EventArgs e)
        {
            // unleash weapon
            mana_spend(10);
            boss_take_dmg(5);
            cd_3 = 2;
            burst = true;
            but3.Visible = false;
            refresh();
        }

        private void but4_Click(object sender, EventArgs e)
        {
            // heal
            mana_spend(30);
            int heal=0;
            if (burst == true)
            {
                heal = 40;
            }
            else
            {
                heal = 20;
            }
            if (w.hp_player + heal >= 100)
            {
                w.hp_player = 100;
            }
            else
            {
                w.hp_player += heal;
            }
            cd_4 = 3;
            num_show(heal, 2);
            but4.Visible = false;
            refresh();
        }

        private void but5_Click(object sender, EventArgs e)
        {
            // light
            mana_spend(5);
            boss_take_dmg(5);
            cd_5 = 1;
            but5.Visible = false;
            refresh();
        }

        public void player_turn()
        {
            // cd reload
            if (cd_1 > 0)  cd_1--;
            if (cd_2 > 0)  cd_2--;
            if (cd_3 > 0)  cd_3--;
            if (cd_4 > 0)  cd_4--;
            if (cd_5 > 0)  cd_5--;
            
            if (cd_1 == 0) { but1.Visible = true; }
            if (cd_2 == 0) { but2.Visible = true; }
            if (cd_3 == 0) { but3.Visible = true; }
            if (cd_4 == 0) { but4.Visible = true; }
            if (cd_5 == 0) { but5.Visible = true; }
            burst = false;

            Random rnd = new Random();
            int chance = rnd.Next(0, 100);
            int dmg = 0;

            // boss turn
            if (chance > 30 && chance < 80)
            {
                dmg = rnd.Next(5, 20);
            } else if (chance < 30)
            {
                dmg = rnd.Next(20, 40);
            } else if (chance >80)
            {
                if (w.hp_boss + 30 >= 100) { w.hp_boss = 100; } else { w.hp_boss += 30; }
            }


            // boss damage
            if (w.hp_player - dmg <= 0)
            {
                w.hp_player = 0;
                pic_boss.Visible = false;
                pic_def.Visible = true;
                timer_msg.Enabled = true;
            }
            else
            {
                w.hp_player -= dmg;
            }

            // mana regen
            if (w.mana_player + 10 >= 100)
            {
                w.mana_player = 100;
            } else { w.mana_player += 10; }
            refresh();
        }

        private void end_turn_Click(object sender, EventArgs e)
        {
            player_turn();
        }

        private void timer_msg_Tick(object sender, EventArgs e)
        {
            pic_vic.Visible = false;
            pic_def.Visible = false;
            timer_msg.Enabled = false;
            this.Enabled = true;

            start();
        }

        private void timer_num_Tick(object sender, EventArgs e)
        {
            num.Visible = false;
        }
    }
}

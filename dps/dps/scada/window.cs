﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace scada
{
    public class window
    {
        public Timer tm;

        public int lvl { get; protected set; }
        public int hp_player { get;  set; }
        public int mana_player { get;  set; }

        public int hp_boss { get;  set; }



        public window()
        {
            hp_player = 100;
            mana_player = 100;
            hp_boss = 100;
        }

        public void DoReset()
        {
            hp_player = 100;
            mana_player = 100;
            lvl = 1;
            hp_boss = 100;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace mobilelab
{
    public partial class MainPage : ContentPage
    {
        int a = 0;
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnButtonClicked(object sender, EventArgs args)
        {
            (sender as Button).Text = a++.ToString();
            await label.RelRotateTo(360, 1000);
        }
    }
}

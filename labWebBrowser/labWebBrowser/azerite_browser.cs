﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labWebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            string logs = "";
            InitializeComponent();
            //
            bgo.Click += delegate
            {
                web.Navigate(address.Text);
            };
            bback.Click += delegate
            {
                web.GoBack();
            };
            bforward.Click += delegate
            {
                web.GoForward();
            };
            breload.Click += delegate
            {
                web.Refresh();
            };
            bstop.Click += delegate
            {
                web.Stop();
            };
            web.DocumentCompleted += delegate
            {

                logs += "\r\n" + address.Text;
                address.Text = web.Url.ToString();
            };
            blogs.Click += delegate
            {
                MessageBox.Show(logs);
            };
        }
    }
}

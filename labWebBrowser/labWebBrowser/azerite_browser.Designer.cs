﻿namespace labWebBrowser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.address = new System.Windows.Forms.TextBox();
            this.bgo = new System.Windows.Forms.Button();
            this.web = new System.Windows.Forms.WebBrowser();
            this.bback = new System.Windows.Forms.Button();
            this.bforward = new System.Windows.Forms.Button();
            this.breload = new System.Windows.Forms.Button();
            this.bstop = new System.Windows.Forms.Button();
            this.blogs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // address
            // 
            this.address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.address.Location = new System.Drawing.Point(12, 12);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(624, 20);
            this.address.TabIndex = 0;
            this.address.Text = "https://ya.ru/";
            // 
            // bgo
            // 
            this.bgo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bgo.AutoSize = true;
            this.bgo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bgo.Location = new System.Drawing.Point(642, 9);
            this.bgo.Name = "bgo";
            this.bgo.Size = new System.Drawing.Size(75, 23);
            this.bgo.TabIndex = 1;
            this.bgo.Text = "Go";
            this.bgo.UseVisualStyleBackColor = false;
            // 
            // web
            // 
            this.web.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.web.Location = new System.Drawing.Point(12, 38);
            this.web.MinimumSize = new System.Drawing.Size(20, 20);
            this.web.Name = "web";
            this.web.Size = new System.Drawing.Size(705, 421);
            this.web.TabIndex = 2;
            // 
            // bback
            // 
            this.bback.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bback.Location = new System.Drawing.Point(12, 465);
            this.bback.Name = "bback";
            this.bback.Size = new System.Drawing.Size(109, 36);
            this.bback.TabIndex = 3;
            this.bback.Text = "<< Back";
            this.bback.UseVisualStyleBackColor = false;
            // 
            // bforward
            // 
            this.bforward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bforward.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bforward.Location = new System.Drawing.Point(127, 465);
            this.bforward.Name = "bforward";
            this.bforward.Size = new System.Drawing.Size(109, 36);
            this.bforward.TabIndex = 4;
            this.bforward.Text = "Forward >>";
            this.bforward.UseVisualStyleBackColor = false;
            // 
            // breload
            // 
            this.breload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.breload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.breload.Location = new System.Drawing.Point(242, 465);
            this.breload.Name = "breload";
            this.breload.Size = new System.Drawing.Size(109, 36);
            this.breload.TabIndex = 5;
            this.breload.Text = "Reload";
            this.breload.UseVisualStyleBackColor = false;
            // 
            // bstop
            // 
            this.bstop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bstop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bstop.Location = new System.Drawing.Point(357, 465);
            this.bstop.Name = "bstop";
            this.bstop.Size = new System.Drawing.Size(109, 36);
            this.bstop.TabIndex = 6;
            this.bstop.Text = "Stop";
            this.bstop.UseVisualStyleBackColor = false;
            // 
            // blogs
            // 
            this.blogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.blogs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.blogs.Location = new System.Drawing.Point(472, 465);
            this.blogs.Name = "blogs";
            this.blogs.Size = new System.Drawing.Size(109, 36);
            this.blogs.TabIndex = 8;
            this.blogs.Text = "show logs";
            this.blogs.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(729, 513);
            this.Controls.Add(this.blogs);
            this.Controls.Add(this.bstop);
            this.Controls.Add(this.breload);
            this.Controls.Add(this.bforward);
            this.Controls.Add(this.bback);
            this.Controls.Add(this.web);
            this.Controls.Add(this.bgo);
            this.Controls.Add(this.address);
            this.MinimumSize = new System.Drawing.Size(490, 550);
            this.Name = "Form1";
            this.Text = "Azerite browser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Button bgo;
        private System.Windows.Forms.WebBrowser web;
        private System.Windows.Forms.Button bback;
        private System.Windows.Forms.Button bforward;
        private System.Windows.Forms.Button breload;
        private System.Windows.Forms.Button bstop;
        private System.Windows.Forms.Button blogs;
    }
}

